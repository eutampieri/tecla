FROM rust:latest AS builder
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN cargo build --release -p tecla_websrv

FROM ubuntu:noble
RUN mkdir /app
WORKDIR /app
COPY --from=builder /app/target/release/tecla_websrv /app/tecla_websrv
RUN apt-get update && apt-get install -y ca-certificates tzdata && apt clean && rm -rf /var/cache/apt

ENV BIND_ADDR=0.0.0.0:8080

CMD /app/tecla_websrv
EXPOSE 8080
