use self::timetable::TimeTableEntry;
use chrono::prelude::*;
use itertools::Itertools;
use serde::Deserialize;

fn time_of_day_to_seconds(time: &str, separator: char) -> i64 {
    time.split(separator)
        .filter_map(|x| x.parse::<i64>().ok())
        .fold((3600, 0), |mut acc, x| {
            acc.1 = acc.1 + x * acc.0;
            acc.0 /= 60;
            acc
        })
        .1
}

#[cfg(test)]
mod tests {
    use crate::entities::time_of_day_to_seconds;

    #[test]
    fn test_hour_minutes() {
        assert_eq!(time_of_day_to_seconds("10:00", ':'), 36000);
    }
    #[test]
    fn test_hour_minutes_seconds() {
        assert_eq!(time_of_day_to_seconds("10:01:10", ':'), 36070);
    }
    #[test]
    fn test_bogus() {
        assert_eq!(time_of_day_to_seconds(" 10:a:10", ':'), 36000);
    }
}

#[derive(Debug, Deserialize)]
pub struct Course {
    pub elenco_anni: Vec<AcademicYear>,
    pub label: String,
    pub tipo: String,
    pub valore: String,
    //pub cdl_id: String,
    pub scuola: String,
    //pub pub_type: String,
    //pub default_grid: String,
    //pub periodi: Vec<Period>,
    //pub facolta_id: String,
    //pub facolta_code: String,
}

#[derive(Debug, Deserialize)]
pub struct Period {
    label: String,
    valore: String,
    r#pub: String,
    id: String,
    aa_id: String,
    facolta_code: String,
}

#[derive(Debug, Deserialize)]
pub struct AcademicYear {
    pub label: String,
    pub valore: String,
    //pub order_lbl: String,
    //pub external: i16,
    #[serde(default)]
    pub elenco_insegnamenti: Vec<Teaching>,
}

impl AcademicYear {
    pub fn get_year(&self) -> u8 {
        self.valore
            .split('|')
            .nth(1)
            .unwrap()
            .parse()
            .unwrap_or_else(|_| 1)
    }
}

#[derive(Debug, Deserialize)]
pub struct Teaching {
    label: String,
    valore: String,
    id: String,
    id_periodo: i16,
    docente: String,
}

pub mod timetable {
    use serde::Deserialize;

    #[derive(Debug, Deserialize)]
    pub struct TimeTable {
        pub celle: Vec<TimeTableEntry>,
    }
    #[derive(Debug, Deserialize)]
    pub struct TimeTableEntry {
        pub codice_insegnamento: String,
        pub nome_insegnamento: String,
        pub docente: String,
        #[serde(default)]
        pub mail_docente: Option<String>,
        pub timestamp: i64,
        pub aula: String,
        pub ora_fine: String,
        pub ora_inizio: String,
    }
}

impl From<&TimeTableEntry> for tecla_common::Teacher {
    fn from(entry: &TimeTableEntry) -> Self {
        Self {
            name: entry.docente.clone(),
            email: entry.mail_docente.clone(),
        }
    }
}

impl From<&TimeTableEntry> for tecla_common::Lesson {
    fn from(entry: &TimeTableEntry) -> Self {
        Self {
            start: chrono::Local.timestamp(entry.timestamp, 0).into(),
            end: chrono::Local
                .timestamp(
                    entry.timestamp + time_of_day_to_seconds(&entry.ora_fine, ':')
                        - time_of_day_to_seconds(&entry.ora_inizio, ':'),
                    0,
                )
                .into(),
            teacher: Some(entry.into()),
            venue: Some(entry.aula.clone()),
            teaching: entry.into(),
            online_class_url: None,
        }
    }
}

impl From<&TimeTableEntry> for tecla_common::Teaching {
    fn from(entry: &TimeTableEntry) -> Self {
        Self {
            id: entry.codice_insegnamento.clone(),
            name: entry.nome_insegnamento.clone(),
        }
    }
}

impl From<&Teaching> for tecla_common::Teaching {
    fn from(teaching: &Teaching) -> Self {
        Self {
            id: teaching.valore.clone(),
            name: teaching.label.clone(),
        }
    }
}

impl From<&Course> for tecla_common::Area {
    fn from(course: &Course) -> Self {
        let name = if course.scuola == "" {
            "Default"
        } else {
            course.scuola.as_str()
        };
        Self {
            id: name.to_lowercase().replace(' ', "-"),
            name: name.to_string(),
        }
    }
}

impl From<&Course> for tecla_common::Course {
    fn from(course: &Course) -> Self {
        let area: tecla_common::Area = course.into();
        Self {
            id: course.valore.clone(),
            name: format!("{} - {}", course.label, course.tipo),
            area_id: area.id,
            duration_in_years: course
                .elenco_anni
                .iter()
                .map(|x| x.get_year())
                .unique()
                .max()
                .unwrap_or_default(),
        }
    }
}

impl From<&AcademicYear> for tecla_common::Curriculum {
    fn from(ac_yr: &AcademicYear) -> Self {
        Self {
            id: ac_yr.valore.split('|').next().unwrap().to_string(),
            name: ac_yr
                .label
                .split(" - ")
                .nth(1)
                .unwrap_or_else(|| ac_yr.label.as_str())
                .to_string(),
        }
    }
}
