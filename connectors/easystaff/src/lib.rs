use itertools::Itertools;
mod entities;

pub struct EasyStaffConnector {
    base_url: &'static str,
    id: &'static str,
    uni_name: &'static str,
    agent: ureq::Agent,
    courses: Vec<entities::Course>,
    last_update: Option<std::time::Instant>,
    combo_call: &'static str,
    grid_call: &'static str,
}

impl EasyStaffConnector {
    pub fn new(
        base_url: &'static str,
        id: &'static str,
        uni_name: &'static str,
        combo_call: &'static str,
        grid_call: &'static str,
    ) -> Self {
        Self {
            base_url,
            id,
            uni_name,
            agent: ureq::builder()
                .user_agent(&format!(
                    "{}/{}",
                    env!("CARGO_PKG_NAME"),
                    env!("CARGO_PKG_VERSION"),
                ))
                .build(),
            courses: vec![],
            last_update: None,
            combo_call,
            grid_call,
        }
    }

    fn parse_js(&self, js: &str) -> std::collections::HashMap<String, String> {
        let regex =
            regex::Regex::new(r"var\s(?P<identifier>[[:graph:]][^\s]*)\s*=\s*(?P<value>.*);")
                .unwrap();
        regex
            .captures_iter(js)
            .map(|x| (x["identifier"].trim().to_string(), x["value"].to_string()))
            .collect()
    }

    fn fetch_aa(&self) -> String {
        let url = format!("{}/{}?aa=1", self.base_url, self.combo_call);
        let values = self.parse_js(&self.agent.get(&url).call().unwrap().into_string().unwrap());
        let ac_yrs: std::collections::BTreeMap<String, std::collections::HashMap<String, String>> =
            serde_json::from_str(&values.get("anni_accademici_ec").expect("Malformed data"))
                .unwrap();
        ac_yrs
            .into_iter()
            .last()
            .map(|(_, x)| x.get("valore").unwrap().to_string())
            .expect("Malformed data")
            .clone()
    }

    fn get_courses(&self) -> Vec<entities::Course> {
        let response = self
            .agent
            .get(&format!(
                "{}/{}?aa={}&page=corsi&sw=ec_",
                self.base_url,
                self.combo_call,
                self.fetch_aa()
            ))
            .call()
            .unwrap()
            .into_string()
            .unwrap();
        let parsed = self.parse_js(&response);
        serde_json::from_str(parsed.get("elenco_corsi").expect("Malformed input")).unwrap_or_else(
            |_| {
                use serde::Deserialize;
                #[derive(Deserialize)]
                struct Resp {
                    elenco: Vec<entities::Course>,
                }
                let parsed: Vec<Resp> =
                    serde_json::from_str(parsed.get("elenco_corsi").expect("Malformed input"))
                        .expect("Malformed data");
                parsed
                    .into_iter()
                    .map(|x| x.elenco)
                    .fold(vec![], |mut a, mut v| {
                        a.append(&mut v);
                        a
                    })
            },
        )
    }
}

impl tecla_common::Connector for EasyStaffConnector {
    fn get_areas(&self) -> Vec<tecla_common::Area> {
        self.courses
            .iter()
            .unique_by(|x| x.scuola.clone())
            .map(|x| x.into())
            .collect()
    }

    fn get_id(&self) -> &'static str {
        self.id
    }

    fn get_institution_name(&self) -> &str {
        self.uni_name
    }

    fn update(&mut self) {
        if self
            .last_update
            .map(|x| std::time::Instant::now().duration_since(x).as_secs() > 3600 * 24)
            .unwrap_or_else(|| true)
        {
            self.courses = self.get_courses();
            self.last_update = Some(std::time::Instant::now());
        }
    }

    fn get_courses(&self) -> Vec<tecla_common::Course> {
        self.courses.iter().map(|x| x.into()).collect()
    }

    fn get_courses_with_area(&self, area_id: &str) -> Vec<tecla_common::Course> {
        self.courses
            .iter()
            .map(|x| x.into())
            .filter(|x: &tecla_common::Course| x.area_id == area_id)
            .collect()
    }

    fn get_curricula_for_course(&self, course_id: &str) -> Vec<tecla_common::Curriculum> {
        self.courses
            .iter()
            .find(|x| x.valore == course_id)
            .map(|y| {
                y.elenco_anni
                    .iter()
                    .map(|x| x.into())
                    .unique_by(|x: &tecla_common::Curriculum| x.id.clone())
                    .map(|x| tecla_common::Curriculum {
                        id: format!("{}^{}", y.valore, x.id),
                        name: x.name,
                    })
                    .collect()
            })
            .unwrap_or_default()
    }

    fn get_teachings_for_curriculum(
        &self,
        curriculum_id: &str,
        year: u8,
    ) -> Vec<tecla_common::Teaching> {
        let mut curriculum_pieces = curriculum_id.split('^');
        let cdl_id = curriculum_pieces.next().unwrap();
        let curriculum_id = curriculum_pieces.next().unwrap_or_default();
        self.courses
            .iter()
            .find(|x| x.valore == cdl_id)
            .map(|x| {
                x.elenco_anni
                    .iter()
                    .find(|x| x.valore == format!("{}|{}", curriculum_id, year))
                    .map(|x| {
                        x.elenco_insegnamenti
                            .iter()
                            .map(|x| x.into())
                            .collect::<Vec<tecla_common::Teaching>>()
                    })
            })
            .flatten()
            .unwrap_or_default()
    }

    fn get_timetable_with_teachings(
        &self,
        curriculum_id: &str,
        teachings: &[&str],
        year: u8,
    ) -> Vec<tecla_common::Lesson> {
        use chrono::prelude::*;

        let mut curriculum_pieces = curriculum_id.split('^');
        let cdl_id = curriculum_pieces.next().unwrap();
        let curriculum_id = curriculum_pieces.next().unwrap_or_default();
        let teachings = teachings
            .into_iter()
            .map(|x| x.to_string())
            .collect::<std::collections::HashSet<_>>();
        let mut timetable = vec![];
        let mut date = chrono::Local::now().date();
        date = date - chrono::Duration::days(date.weekday().num_days_from_monday() as i64);
        let mut timetable_length = 0;
        while (timetable.len() == 0 || timetable_length > 0)
            && (date - chrono::Local::now().date()).num_days() < 120
        {
            if let Ok(mut timetable_part) = self
                .agent
                .post(&format!("{}/{}", self.base_url, self.grid_call))
                .send_form(&[
                    ("view", "easycourse"),
                    ("form-type", "corso"),
                    ("include", "corso"),
                    ("anno", &self.fetch_aa()),
                    ("corso", cdl_id),
                    ("visualizzazione_orario", "cal"),
                    ("anno2[]", &format!("{}|{}", curriculum_id, year)),
                    ("date", date.format("%d-%m-%Y").to_string().as_str()),
                ])
                .unwrap()
                .into_json::<entities::timetable::TimeTable>()
            {
                date = date + chrono::Duration::days(7);
                timetable_length = timetable_part.celle.len();
                timetable.append(&mut timetable_part.celle);
            } else {
                date = date + chrono::Duration::days(7);
                continue;
            }
        }
        timetable
            .iter()
            .filter(|x| teachings.contains(x.codice_insegnamento.as_str()))
            .map(|x| x.into())
            .collect()
    }

    fn get_data_license(&self) -> Option<Box<dyn tecla_common::license::License>> {
        None
    }
}
