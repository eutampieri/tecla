use tecla_common::{Lesson, Teaching};

mod utils;

lazy_static::lazy_static! {
    static ref AGENT: ureq::Agent = ureq::builder()
    .user_agent(&format!(
        "{}/{}",
        env!("CARGO_PKG_NAME"),
        git_version::git_version!()
    ))
    .build();
}

pub struct OrarioFacile<U>
where
    U: Copy
        + FnOnce(
            &ureq::Agent,
        ) -> (
            Vec<tecla_common::Area>,
            Vec<tecla_common::Course>,
            Vec<(String, tecla_common::Curriculum)>,
            std::collections::HashMap<(String, u8), String>,
        ),
{
    id: &'static str,
    name: &'static str,
    get_groups: U,
    areas: Vec<tecla_common::Area>,
    courses: Vec<tecla_common::Course>,
    curricula: Vec<(String, tecla_common::Curriculum)>,
    urls: std::collections::HashMap<(String, u8), String>,
    timetables: std::collections::HashMap<(String, u8), (Vec<Lesson>, Vec<Teaching>)>,
}

impl<
        U: Copy
            + FnOnce(
                &ureq::Agent,
            ) -> (
                Vec<tecla_common::Area>,
                Vec<tecla_common::Course>,
                Vec<(String, tecla_common::Curriculum)>,
                std::collections::HashMap<(String, u8), String>,
            ),
    > OrarioFacile<U>
{
    fn parse_orariofacile(&self, orario: &str) -> (Vec<Lesson>, Vec<Teaching>) {
        if let Ok(tree) = html_parser::Dom::parse(dbg!(orario)) {
            dbg!(tree.children);
            (vec![], vec![])
        } else {
            (vec![], vec![])
        }
    }
}

impl<
        U: Send
            + Sync
            + Copy
            + FnOnce(
                &ureq::Agent,
            ) -> (
                Vec<tecla_common::Area>,
                Vec<tecla_common::Course>,
                Vec<(String, tecla_common::Curriculum)>,
                std::collections::HashMap<(String, u8), String>,
            ),
    > tecla_common::Connector for OrarioFacile<U>
{
    fn get_areas(&self) -> Vec<tecla_common::Area> {
        self.areas.clone()
    }

    fn get_id(&self) -> &'static str {
        self.id
    }

    fn get_institution_name(&self) -> &str {
        self.name
    }

    fn update(&mut self) {
        let (areas, courses, curricula, urls) = (self.get_groups)(&AGENT);
        self.areas = areas;
        self.courses = courses;
        self.curricula = curricula;
        self.urls = urls;
        self.timetables = self
            .urls
            .iter()
            .map(|(k, v)| {
                (
                    k.clone(),
                    match AGENT.get(v).call().map(|x| x.into_string()) {
                        Ok(r) => match r {
                            Ok(str) => self.parse_orariofacile(&str),
                            _ => (vec![], vec![]),
                        },
                        Err(_) => (vec![], vec![]),
                    },
                )
            })
            .collect()
    }

    fn get_courses(&self) -> Vec<tecla_common::Course> {
        self.courses.clone()
    }

    fn get_courses_with_area(&self, area_id: &str) -> Vec<tecla_common::Course> {
        self.courses
            .iter()
            .filter(|x| x.area_id == area_id)
            .cloned()
            .collect()
    }

    fn get_curricula_for_course(&self, course_id: &str) -> Vec<tecla_common::Curriculum> {
        self.curricula
            .iter()
            .filter(|x| x.0 == course_id)
            .map(|(_, x)| x)
            .cloned()
            .collect()
    }

    fn get_teachings_for_curriculum(
        &self,
        curriculum_id: &str,
        year: u8,
    ) -> Vec<tecla_common::Teaching> {
        if let Some(schedule) = self.timetables.get(&(curriculum_id.to_string(), year)) {
            schedule.1.iter().cloned().collect()
        } else {
            vec![]
        }
    }

    fn get_timetable_with_teachings(
        &self,
        curriculum_id: &str,
        teachings: &[&str],
        year: u8,
    ) -> Vec<tecla_common::Lesson> {
        let teachings = teachings
            .iter()
            .map(|x| x.to_string())
            .collect::<std::collections::HashSet<_>>();
        if let Some(schedule) = self.timetables.get(&(curriculum_id.to_string(), year)) {
            schedule
                .0
                .iter()
                .filter(|x| teachings.contains(x.teaching.id.as_str()))
                .cloned()
                .collect()
        } else {
            vec![]
        }
    }

    fn get_data_license(&self) -> Option<Box<dyn tecla_common::license::License>> {
        None
    }
}

#[cfg(test)]
mod tests {
    use crate::OrarioFacile;

    #[test]
    fn it_works() {
        let of = OrarioFacile {
            id: "a",
            name: "b",
            get_groups: |_: &ureq::Agent| {
                (
                    vec![],
                    vec![],
                    vec![],
                    std::collections::HashMap::<_, _>::new(),
                )
            },
            areas: vec![],
            courses: vec![],
            curricula: vec![],
            urls: Default::default(),
            timetables: Default::default(),
        };
        of.parse_orariofacile(
            super::AGENT
                .get("http://servizi.alberghetti.it/orario/itis/Classi/2ALS.html")
                .call()
                .unwrap()
                .into_string()
                .unwrap()
                .as_str(),
        );
        assert_eq!(2 + 2, 4);
    }
}
