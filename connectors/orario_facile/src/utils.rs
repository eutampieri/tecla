use html_parser::Node;

pub fn find_table_children<'a>(node: &'a Node) -> Option<&'a [html_parser::Node]> {
    match dbg!(node) {
        Node::Text(_) => None,
        Node::Element(el) => {
            if el.name.to_lowercase() == "table" {
                Some(el.children.as_ref())
            } else {
                el.children.iter().find_map(find_table_children)
            }
        }
        Node::Comment(_) => None,
    }
}
