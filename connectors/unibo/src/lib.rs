use chrono::prelude::*;
use chrono_tz::Europe::Rome;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use tecla_common::{Connector, Course, Curriculum};

type UniboCourse = (String, u32, [u8; 16], String, u8);

const DATA_VALIDITY_IN_SECS: u64 = 24 * 3_600;

lazy_static::lazy_static! {
    static ref AGENT: ureq::Agent = ureq::builder()
    .user_agent(&format!(
        "{}/{}",
        env!("CARGO_PKG_NAME"),
        git_version::git_version!()
    ))
    .build();
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct TimeTableVenue {
    des_risorsa: String,
    des_indirizzo: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct TimeTableEntry {
    cod_modulo: String,
    title: String,
    #[serde(default)]
    teams: Option<String>,
    #[serde(default)]
    docente: String,
    start: String,
    end: String,
    #[serde(default)]
    aule: Vec<TimeTableVenue>,
}

impl From<&TimeTableEntry> for tecla_common::Teaching {
    fn from(tte: &TimeTableEntry) -> Self {
        Self {
            id: format!("unibo.t.{}", tte.cod_modulo.clone()),
            name: tte.title.clone(),
        }
    }
}

impl From<&TimeTableEntry> for tecla_common::Lesson {
    fn from(tte: &TimeTableEntry) -> Self {
        Self {
            start: Rome
                .datetime_from_str(&tte.start, "%FT%T")
                .unwrap()
                .with_timezone(&chrono::Utc),
            end: Rome
                .datetime_from_str(&tte.end, "%FT%T")
                .unwrap()
                .with_timezone(&chrono::Utc),
            teacher: Some(tecla_common::Teacher {
                name: tte.docente.clone(),
                email: None,
            }),
            venue: tte
                .aule
                .first()
                .map(|x| format!("{} - {}", x.des_risorsa, x.des_indirizzo)),
            teaching: tte.into(),
            online_class_url: tte.teams.clone(),
        }
    }
}

fn extract_numerical_id<T: std::str::FromStr>(string: &str, default: T) -> T {
    string
        .split('.')
        .last()
        .unwrap_or_default()
        .parse::<T>()
        .unwrap_or(default)
}

/// Map the course URL to the corresponding timetable URL
fn get_timetable_denomination(url: &str) -> &'static str {
    let mut pieces = url.split('/');
    match pieces.nth(3) {
        Some(p) => match p {
            "magistralecu" => "orario-lezioni",
            "magistrale" => "orario-lezioni",
            "laurea" => "orario-lezioni",
            "singlecycle" => "timetable",
            "1cycle" => "timetable",
            "2cycle" => "timetable",
            _ => "orario-lezioni",
        },
        None => "",
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct UniboConnector {
    last_update: std::time::SystemTime,
    areas: Vec<String>,
    courses: Vec<UniboCourse>,
    curricula_cache: std::sync::RwLock<HashMap<String, Vec<Curriculum>>>,
    urls_cache: std::sync::RwLock<HashMap<u32, String>>,
    timetable_cache: std::sync::RwLock<HashMap<String, Vec<TimeTableEntry>>>,
    emails_cache: std::sync::RwLock<HashMap<String, Option<String>>>,
}
impl Default for UniboConnector {
    fn default() -> Self {
        Self {
            //last_update: Default::default(),
            last_update: std::time::UNIX_EPOCH,
            areas: Default::default(),
            courses: Default::default(),
            curricula_cache: Default::default(),
            urls_cache: Default::default(),
            timetable_cache: Default::default(),
            emails_cache: Default::default(),
        }
    }
}

impl UniboConnector {
    /// Fetch a teacher's email given their name
    pub fn teacher_email_lookup(&self, name: &str) -> Option<String> {
        let hm = self.emails_cache.read().ok()?;
        if let Some(res) = hm.get(name) {
            res.clone()
        } else {
            drop(hm);
            let page = AGENT.get(&format!("https://www.unibo.it/uniboWeb/unibosearch/rubrica.aspx?tab=FullTextPanel&query={}&tipo=people", name.replace(' ',"+"))).call().ok()?.into_string().ok()?;
            let selector = scraper::Selector::parse(".email").ok()?;
            let doc = scraper::Html::parse_document(&page);
            let res = doc
                .select(&selector)
                .next()
                .map(|e| e.inner_html().trim().to_string());
            let mut hm = self.emails_cache.write().ok()?;
            hm.insert(name.to_string(), res.clone());
            res
        }
    }

    fn get_binary_representation(&self) -> Vec<u8> {
        bincode::serialize(&self).unwrap()
    }

    /// Get the current open data URL
    fn get_opendata_url() -> String {
        #[derive(Deserialize)]
        struct ODResp {
            result: ODRes,
        }
        #[derive(Deserialize)]
        struct ODRes {
            resources: Vec<ODRsc>,
        }
        #[derive(Deserialize)]
        struct ODRsc {
            url: String,
        }

        let resp: ODResp = AGENT
            .get("https://dati.unibo.it/api/3/action/package_show?id=degree-programmes")
            .call()
            .unwrap()
            .into_json()
            .unwrap();
        resp.result.resources[0].url.clone()
    }

    /// Fetch all courses from the Open Data
    fn fetch_courses() -> (Vec<String>, Vec<UniboCourse>) {
        #[derive(Debug, Deserialize)]
        struct Row {
            annoaccademico: String,
            immatricolabile: String,
            corso_codice: String,
            corso_descrizione: String,
            url: String,
            campus: String,
            sededidattica: String,
            ambiti: String,
            tipologia: String,
            durata: String,
            internazionale: String,
            internazionale_titolo: String,
            internazionale_lingua: String,
            lingue: String,
            accesso: String,
        }
        let csv = AGENT
            .get(&Self::get_opendata_url())
            .call()
            .unwrap()
            .into_string()
            .unwrap();
        let mut rdr = csv::Reader::from_reader(csv.as_bytes());
        let data: Vec<Row> = rdr.deserialize().filter_map(|x| x.ok()).collect();
        let areas_set = data
            .iter()
            .map(|x| x.ambiti.clone())
            .collect::<std::collections::HashSet<_>>();

        let mut areas = areas_set.iter().cloned().collect::<Vec<_>>();
        areas.sort();
        let courses = data
            .iter()
            .map(|x| {
                (
                    format!(
                        "{} - {} ({})",
                        x.corso_descrizione, x.tipologia, x.sededidattica
                    ),
                    x.corso_codice.parse().unwrap(),
                    md5::compute(x.ambiti.as_bytes()).0,
                    x.url.clone(),
                    x.durata.parse().unwrap(),
                )
            })
            .collect::<Vec<_>>();
        (areas, courses)
    }

    /// Get all courses matching a criteria
    fn get_courses_filtered<F: FnMut(&&UniboCourse) -> bool>(&self, filter: F) -> Vec<Course> {
        self.courses
            .iter()
            .filter(filter)
            .map(|x| Course {
                id: format!("{}.c.{}", self.get_id().replace('.', "-"), x.1),
                name: x.0.clone(),
                area_id: format!("{}.c.{}", self.get_id().replace('.', "-"), x.1),
                duration_in_years: x.4,
            })
            .collect()
    }
    fn get_course_base_url(&self, course_id: &str) -> Option<String> {
        let course_id = extract_numerical_id(course_id, u32::MAX);
        let mut hm = self.urls_cache.write().ok()?;
        if let Some(url) = hm.get(&course_id) {
            Some(url.clone())
        } else {
            let course_url = self
                .courses
                .iter()
                .filter(|x| x.1 == course_id)
                .map(|x| &x.3)
                .next()?;
            let selector = scraper::Selector::parse(".social-contact ul li ul li p a").unwrap();
            let page = scraper::Html::parse_document(
                &AGENT.get(course_url).call().ok()?.into_string().ok()?,
            );
            let url = page
                .select(&selector)
                .next()?
                .value()
                .attr("href")?
                .to_owned();
            hm.insert(course_id, url.clone());
            drop(hm);
            self.save();
            Some(url)
        }
    }
    fn copy_from(&mut self, other: Self) {
        self.last_update = other.last_update;
        self.areas = other.areas;
        self.courses = other.courses;
        self.curricula_cache = other.curricula_cache;
        self.urls_cache = other.urls_cache;
        self.timetable_cache = other.timetable_cache;
    }
    fn save(&self) {
        std::fs::write(
            format!("tecla_{}.sav", self.get_id().replace('.', "-")),
            &self.get_binary_representation(),
        )
        .unwrap_or_else(|x| eprintln!("{:?}", x));
    }
    fn get_timetable(&self, url: &str) -> Vec<TimeTableEntry> {
        let mut hm = self.timetable_cache.write().expect("Poisoned mutex");
        if let Some(res) = hm.get(url) {
            res.clone()
        } else if let Ok(resp) = AGENT.get(url).call() {
            let entries: Vec<TimeTableEntry> = resp.into_json().unwrap_or_default();
            hm.insert(url.to_string(), entries.clone());
            drop(hm);
            self.save();
            entries
        } else {
            vec![]
        }
    }
}

impl tecla_common::Connector for UniboConnector {
    fn get_areas(&self) -> Vec<tecla_common::Area> {
        self.areas
            .iter()
            .map(|x| tecla_common::Area {
                name: x.clone(),
                id: format!(
                    "{}.a.{:x}",
                    self.get_id().replace('.', "-"),
                    md5::compute(x.as_bytes())
                ),
            })
            .collect()
    }

    fn get_id(&self) -> &'static str {
        "it.unibo"
    }

    fn update(&mut self) {
        if std::time::SystemTime::now()
            .duration_since(self.last_update)
            .map(|x| x.as_secs() > DATA_VALIDITY_IN_SECS)
            .unwrap_or_else(|_| true)
        {
            if let Ok(b) = std::fs::read(format!("tecla_{}.sav", self.get_id().replace('.', "-"))) {
                if self.get_binary_representation() != b {
                    let new_connector: UniboConnector = bincode::deserialize(&b).unwrap();
                    self.copy_from(new_connector);
                } else {
                    let (areas, courses) = Self::fetch_courses();
                    self.copy_from(Default::default());
                    self.areas = areas;
                    self.courses = courses;
                    self.last_update = std::time::SystemTime::now();
                    self.save();
                }
            } else {
                let (areas, courses) = Self::fetch_courses();
                self.areas = areas;
                self.courses = courses;
                self.last_update = std::time::SystemTime::now();
                self.save();
            }
        }
    }

    fn get_courses(&self) -> Vec<tecla_common::Course> {
        self.get_courses_filtered(|_| true)
    }

    fn get_courses_with_area(&self, area_id: &str) -> Vec<tecla_common::Course> {
        let area_id = area_id.split('.').last().unwrap_or_default();
        self.get_courses_filtered(|x| format!("{:x}", md5::Digest(x.2)) == area_id)
    }

    fn get_curricula_for_course(&self, course_id: &str) -> Vec<tecla_common::Curriculum> {
        let hm_ro = self.curricula_cache.read().expect("Poisoned mutex");
        if let Some(entry) = hm_ro.get(course_id) {
            entry.clone()
        } else if let Some(course_url) = self.get_course_base_url(course_id) {
            drop(hm_ro);
            let mut hm = self.curricula_cache.write().expect("Poisoned mutex");
            #[derive(Deserialize)]
            struct Resp {
                value: String,
                label: String,
            }
            let res: Vec<Curriculum> = {
                AGENT
                    .get(&format!(
                        "{}/{}/@@available_curricula",
                        course_url,
                        get_timetable_denomination(&course_url)
                    ))
                    .call()
                    .map(|x| x.into_json::<Vec<Resp>>().unwrap())
                    .unwrap_or_default()
            }
            .into_iter()
            .map(|x| Curriculum {
                id: format!("{}.p.{}", course_id, x.value),
                name: x.label,
            })
            .collect();
            hm.insert(course_id.to_string(), res.clone());
            drop(hm);
            self.save();
            res
        } else {
            vec![]
        }
    }

    fn get_teachings_for_curriculum(
        &self,
        curriculum_id: &str,
        year: u8,
    ) -> Vec<tecla_common::Teaching> {
        let cu_id = curriculum_id.split('.').last();
        let co_id = curriculum_id.split('.').nth(2);
        match (co_id, cu_id) {
            (Some(course), Some(curriculum)) => {
                let base_url = self.get_course_base_url(course).unwrap_or_default();
                let timetable_url = format!(
                    "{}/{}/@@orario_reale_json?anno={}&curricula={}",
                    base_url,
                    get_timetable_denomination(&base_url),
                    year,
                    curriculum
                );
                self.get_timetable(&timetable_url)
                    .into_iter()
                    .map(|x| tecla_common::Teaching::from(&x))
                    .unique_by(|x| x.id.clone())
                    .collect()
            }
            _ => vec![],
        }
    }

    fn get_timetable_with_teachings(
        &self,
        curriculum_id: &str,
        teachings: &[&str],
        year: u8,
    ) -> Vec<tecla_common::Lesson> {
        let cu_id = curriculum_id.split('.').last();
        let co_id = curriculum_id.split('.').nth(2);
        let teaching_set = teachings
            .iter()
            .map(|x| x.to_string())
            .collect::<std::collections::HashSet<_>>();
        match (co_id, cu_id) {
            (Some(course), Some(curriculum)) => {
                let base_url = self.get_course_base_url(course).unwrap_or_default();
                let timetable_url = format!(
                    "{}/{}/@@orario_reale_json?anno={}&curricula={}",
                    base_url,
                    get_timetable_denomination(&base_url),
                    year,
                    curriculum
                );
                self.get_timetable(&timetable_url)
                    .into_iter()
                    .map(|x| tecla_common::Lesson::from(&x))
                    .map(|mut x| {
                        if let Some(t) = x.teacher.as_mut() {
                            t.email = self.teacher_email_lookup(&t.name);
                        }
                        x
                    })
                    .filter(|x| teaching_set.contains(x.teaching.id.as_str()))
                    .collect()
            }
            _ => vec![],
        }
    }

    fn get_institution_name(&self) -> &str {
        "Alma Mater Studiorum Università di Bologna"
    }

    fn get_data_license(&self) -> Option<Box<dyn tecla_common::license::License>> {
        Some(Box::new(tecla_common::license::licenses::CcBy3_0))
    }
}

#[cfg(test)]
mod tests {
    use tecla_common::Connector;

    use crate::UniboConnector;

    #[test]
    fn it_works() {
        let mut unibo = UniboConnector::default();
        unibo.update();
        dbg!(unibo.get_areas());
        dbg!(unibo.get_courses_with_area("unibo.a.4"));
        dbg!(unibo.get_curricula_for_course("unibo.c.8615"));
        dbg!(unibo.get_teachings_for_curriculum("unibo.c.8615.p.000-000", 3));
        dbg!(unibo.get_timetable_with_teachings("unibo.c.8615.p.000-000", &["unibo.t.41731"], 3));
    }
}
