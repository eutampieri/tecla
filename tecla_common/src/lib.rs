pub use license;
use serde::{Deserialize, Serialize};

#[cfg(test)]
mod tests;

/// A grouping of university courses (i.e. engineering, science, maths, ...)
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Area {
    pub id: String,
    pub name: String,
}

/// A specific course (i.e. computer science)
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Course {
    pub id: String,
    pub name: String,
    pub area_id: String,
    pub duration_in_years: u8,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Curriculum {
    pub id: String,
    pub name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Teaching {
    pub id: String,
    pub name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Teacher {
    pub name: String,
    pub email: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Lesson {
    pub start: chrono::DateTime<chrono::Utc>,
    pub end: chrono::DateTime<chrono::Utc>,
    pub teacher: Option<Teacher>,
    pub venue: Option<String>,
    pub teaching: Teaching,
    pub online_class_url: Option<String>,
}

pub trait Connector: Sync + Send {
    /// Obtains all areas of a given university
    fn get_areas(&self) -> Vec<Area>;

    /// Obtains connector ID
    fn get_id(&self) -> &'static str;

    /// Obtains human readable name of the university
    fn get_institution_name(&self) -> &str;

    /// Check for updates from the source
    fn update(&mut self);

    /// Get all courses
    fn get_courses(&self) -> Vec<Course>;

    /// Get all courses for an area
    fn get_courses_with_area(&self, area_id: &str) -> Vec<Course>;

    /// Get all curricula for a course
    fn get_curricula_for_course(&self, course_id: &str) -> Vec<Curriculum>;

    /// Get all teachings for a curriculum
    fn get_teachings_for_curriculum(&self, curriculum_id: &str, year: u8) -> Vec<Teaching>;

    /// Get timetable for given teachings
    fn get_timetable_with_teachings(
        &self,
        curriculum_id: &str,
        teachings: &[&str],
        year: u8,
    ) -> Vec<Lesson>;

    /// Retrieve the license under which fetched data is provided
    fn get_data_license(&self) -> Option<Box<dyn license::License>>;
}
