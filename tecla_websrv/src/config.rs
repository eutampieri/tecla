pub fn get_connectors() -> Vec<Box<dyn tecla_common::Connector + Send + Sync>> {
    vec![
        Box::new(tecla_connector_unibo::UniboConnector::default()),
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://aule.unife.it/AgendaStudenti",
            "it.unife",
            "Università di Ferrara",
            "combo.php",
            "grid_call.php",
        )),
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://gestioneorari.didattica.unimib.it/PortaleStudentiUnimib",
            "it.unimib",
            "Università di Milano Bicocca",
            "combo.php",
            "grid_call.php",
        )),
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://easyacademy.unitn.it/AgendaStudentiUnitn",
            "it.unitn",
            "Università di Trento",
            "combo.php",
            "grid_call.php",
        )),
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://planner.uniud.it/PortaleStudenti",
            "it.uniud",
            "Università di Udine",
            "combo.php",
            "grid_call.php",
        )),
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://planning.sns.it/AgendaWeb",
            "it.sns",
            "Scuola normale superiore",
            "combo.php",
            "grid_call.php",
        )),
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://unica.easystaff.it/AgendaWeb",
            "it.unica",
            "Università degli studi di Cagliari",
            "combo.php",
            "grid_call.php",
        )),
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://kairos.unifi.it/agendaweb",
            "it.unifi",
            "Università degli studi di Firenze",
            "combo.php",
            "grid_call.php",
        )),
        /*Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "http://85.10.250.146/AgendaWeb/Unicampus/Orario",
            "it.unicampus",
            "Università Campus Bio-Medico di Roma",
            "combo.php",
            "grid_call.php",
        )),
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://aule.univpm.it/agendastudenti",
            "it.univpm",
            "Università Politecnica delle Marche",
            "combo_call_new.php",
            "grid_call_new.php",
        )), Won't support*/
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://unierplanner.easystaff.it/agendaweb_unier",
            "it.universitaeuropeadiroma",
            "Università Europea di Roma",
            "combo.php",
            "grid_call.php",
        )),
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://classroom.ssmlcarlobo.it/timetabling",
            "it.ssmlcarlobo",
            "SSML Istituto di Alti Studi Carlo Bo",
            "combo.php",
            "grid_call.php",
        )),
        Box::new(tecla_connector_easystaff::EasyStaffConnector::new(
            "https://logistica.univr.it/PortaleStudentiUnivr",
            "it.univr",
            "Università di Verona",
            "combo.php",
            "grid_call.php",
        )),
    ]
}
