use actix_web::{get, App, HttpResponse, HttpServer, Responder};
mod config;
mod routes;

pub type Connectors = std::sync::Arc<
    std::sync::RwLock<
        std::collections::HashMap<String, Box<dyn tecla_common::Connector + Send + Sync>>,
    >,
>;

fn get_connectors() -> Connectors {
    let connectors = config::get_connectors();
    let lock = std::sync::Arc::new(std::sync::RwLock::new(
        connectors
            .into_iter()
            .map(|x| (x.get_id().to_string(), x))
            .collect::<std::collections::HashMap<_, _>>(),
    ));
    let update_lock = lock.clone();
    std::thread::spawn(move || loop {
        match update_lock.try_write() {
            Ok(mut hm) => {
                for c in (*hm).values_mut() {
                    c.update();
                }
            }
            Err(e) => {
                eprintln!("{:?}", e);
                std::thread::sleep(std::time::Duration::from_secs(5));
                continue;
            }
        }
        std::thread::sleep(std::time::Duration::from_secs(3600));
    });
    lock
}

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let connectors = get_connectors();
    HttpServer::new(move || {
        App::new()
            .data(connectors.clone())
            .service(hello)
            .service(routes::uni_list)
            .service(routes::get_areas_for_uni)
            .service(routes::get_courses_for_area)
            .service(routes::get_curricula)
            .service(routes::get_teachings)
            .service(routes::get_timetable)
    })
    .bind(std::env::var("BIND_ADDR").unwrap_or("127.0.0.1:8080".to_string()))?
    .run()
    .await
}
