use actix_web::{get, web, Responder};
use serde::{Deserialize, Serialize};

use serde::de::{Deserializer, MapAccess, Visitor};
use std::fmt;

impl<'de> Deserialize<'de> for TimetableTeachings {
    fn deserialize<D>(deserializer: D) -> Result<TimetableTeachings, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct FieldVisitor;

        impl<'de> Visitor<'de> for FieldVisitor {
            type Value = TimetableTeachings;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("`id`")
            }

            fn visit_map<V>(self, mut map: V) -> Result<TimetableTeachings, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut ids: Vec<String> = Vec::default();
                while let Some(key) = map.next_key()? {
                    match key {
                        "teachings" => ids.push(map.next_value::<String>()?),
                        _ => (),
                    }
                }
                Ok(TimetableTeachings { teachings: ids })
            }
        }
        deserializer.deserialize_identifier(FieldVisitor)
    }
}
pub struct TimetableTeachings {
    teachings: Vec<String>,
}

#[get("/unis")]
pub async fn uni_list(connectors: web::Data<super::Connectors>) -> impl Responder {
    #[derive(Serialize)]
    struct University {
        pub name: String,
        pub id: String,
        pub license: String,
    }
    let hm = connectors.read().unwrap();
    web::Json(
        hm.values()
            .into_iter()
            .map(|x| University {
                name: x.get_institution_name().to_string(),
                id: x.get_id().to_string(),
                license: x
                    .get_data_license()
                    .map(|x| x.name().to_string())
                    .unwrap_or_else(|| {
                        format!("© {} - All rights reserved", x.get_institution_name())
                    }),
            })
            .collect::<Vec<_>>(),
    )
}

#[get("/unis/{uni_id}/areas")]
pub async fn get_areas_for_uni(
    web::Path(uni_id): web::Path<String>,
    connectors: web::Data<super::Connectors>,
) -> impl Responder {
    let hm = connectors.read().unwrap();
    if let Some(connector) = hm.get(&uni_id) {
        web::Json(connector.get_areas())
    } else {
        web::Json(vec![])
    }
}

#[get("/unis/{uni_id}/areas/{area_id}/courses")]
pub async fn get_courses_for_area(
    web::Path((uni_id, area_id)): web::Path<(String, String)>,
    connectors: web::Data<super::Connectors>,
) -> impl Responder {
    //dbg!("a");
    let hm = connectors.read().unwrap();
    //dbg!("b");
    if let Some(connector) = hm.get(&uni_id) {
        web::Json(connector.get_courses_with_area(&area_id))
    } else {
        web::Json(vec![])
    }
}

#[get("/unis/{uni_id}/courses/{course_id}/curricula")]
pub async fn get_curricula(
    web::Path((uni_id, course_id)): web::Path<(String, String)>,
    connectors: web::Data<super::Connectors>,
) -> impl Responder {
    let hm = connectors.read().unwrap();
    if let Some(connector) = hm.get(&uni_id) {
        web::Json(
            connector
                .get_curricula_for_course(&urlencoding::decode(&course_id).unwrap_or_default()),
        )
    } else {
        web::Json(vec![])
    }
}

#[get("/unis/{uni_id}/curricula/{curriculum_id}/teachings/{year}")]
pub async fn get_teachings(
    web::Path((uni_id, curriculum_id, year)): web::Path<(String, String, u8)>,
    connectors: web::Data<super::Connectors>,
) -> impl Responder {
    let hm = connectors.read().unwrap();
    if let Some(connector) = hm.get(&uni_id) {
        web::Json(connector.get_teachings_for_curriculum(
            &urlencoding::decode(&curriculum_id).unwrap_or_default(),
            year,
        ))
    } else {
        web::Json(vec![])
    }
}

#[get("/unis/{uni_id}/curricula/{curriculum_id}/timetables/{year}")]
pub async fn get_timetable(
    web::Path((uni_id, curriculum_id, year)): web::Path<(String, String, u8)>,
    teachings: web::Query<TimetableTeachings>,
    connectors: web::Data<super::Connectors>,
) -> impl Responder {
    let hm = connectors.read().unwrap();
    let teachings = teachings
        .teachings
        .iter()
        .map(|x| x.as_str())
        .collect::<Vec<_>>();
    if let Some(connector) = hm.get(&uni_id) {
        web::Json(connector.get_timetable_with_teachings(
            &urlencoding::decode(&curriculum_id).unwrap_or_default(),
            &teachings,
            year,
        ))
    } else {
        web::Json(vec![])
    }
}
